#!/usr/bin/env ruby
# frozen_string_literal: true

require_relative 'database'
require_relative 'slurp'
require_relative 'rate'
require_relative 'report'

VERSION = '1.0'

# Main app class
class Sleasy

  def initialize
    puts "sleasy v#{VERSION}"
    @db = Database.new 'sleasy.db'
  end

  def action
    help if ARGV.length != 1
    case ARGV[0].downcase
    when 'slurp'
      action_slurp
    when 'rate'
      action_rate
    when 'report'
      action_report
    else
      action_help
    end
  end

  private

  def action_help
    puts 'Usage: sleasy [ slurp | rate | report ]'
    exit
  end

  def action_slurp
    slurp = Slurp.new(@db)
    slurp.process
  end

  def action_rate
    rate = Rate.new(@db)
    rate.review
  end

  def action_report
    report = Report.new(@db)
    report.show
  end
end

sleasy = Sleasy.new
sleasy.action
