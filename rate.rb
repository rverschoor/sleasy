# frozen_string_literal: true

require 'tty-prompt'
require 'pastel'

# Rate slurped issues
class Rate

  def initialize(db)
    @db = db
    init_ui
  end

  def review
    loop do
      issue = @db.issue_to_rate
      break if issue.nil?

      review_issue issue
    end
  end

  private

  def init_ui
    @prompt = TTY::Prompt.new
    @pastel = Pastel.new
  end

  def clear_screen
    system 'clear'
  end

  def review_issue(issue)
    clear_screen
    show_issue issue
    rate issue
  end

  def show_issue(issue)
    show_header issue
    show_discussion issue
  end

  def show_header(issue)
    puts @pastel.black.on_green "=== ##{issue['ticket']} #{issue['stamp']} #{issue['title']}"
    puts
  end

  def show_discussion(issue)
    notes = JSON.parse issue['notes']
    notes.each do |note|
      note['notes']['nodes'].each do |comment|
        body = comment['body']
        next if skip_body body

        puts @pastel.green.bold comment['author']['username']
        puts body
        puts
      end
    end
  end

  def skip_body(text)
    text.start_with?(
      'marked the checklist item',
      'changed the description',
      'assigned to @',
      'mentioned in issue',
      'marked this issue as',
      'changed title from'
    ) ||
      text.include?(
        'The staging license has been generated'
      ) ||
      text.include?(
        'of time spent'
      )
  end

  def rate(issue)
    answer = @prompt.select('Rate this issue', %w[Easy Complicated Quit])
    case answer
    when 'Easy'
      @db.rate_easy issue['ticket']
    when 'Complicated'
      @db.rate_complicated issue['ticket']
    else
      exit
    end
  end
end
