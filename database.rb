# frozen_string_literal: true

require 'sqlite3'
require_relative 'issues'

# Database class
class Database
  def initialize(dbname)
    @dbname = dbname
    create_database
    create_tables
  end

  def write_issue(record)
    @issues.write record
  end

  def issue_to_rate
    @db.get_first_row 'select * from issues where easy is null limit 1'
  end

  def rate_easy(ticket)
    rate ticket, true
  end

  def rate_complicated(ticket)
    rate ticket, false
  end

  def count_records
    @db.get_first_value 'select count(ticket) from issues'
  end

  def count_ranked
    @db.get_first_value 'select count(ticket) from issues where easy is not null'
  end

  def count_easy
    @db.get_first_value 'select count(ticket) from issues where easy is 1'
  end

  def count_complex
    @db.get_first_value 'select count(ticket) from issues where easy is 0'
  end

  private

  def create_database
    @db = SQLite3::Database.new @dbname
    @db.results_as_hash = true
  end

  def create_tables
    create_table_issues
  end

  def create_table_issues
    @issues = Issues.new @db
    @issues.create_table
  end

  def rate(ticket, rating)
    @db.execute "update issues set easy = #{rating} where ticket = #{ticket}"
  end
end
