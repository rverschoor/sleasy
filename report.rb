# frozen_string_literal: true

# Report db stats
class Report

  def initialize(db)
    @db = db
  end

  def show
    puts "Total issues: #{@db.count_records}"
    puts "Ranked: #{@db.count_ranked}"
    puts "Easy: #{@db.count_easy}"
    puts "Complex: #{@db.count_complex}"
  end
end