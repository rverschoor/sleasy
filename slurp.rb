# frozen_string_literal: true

require_relative 'graphql'
require_relative 'progress'

# Slurp issues and store in database
class Slurp
  QUERY = <<~GRAPHQL
    query ($cursor: String!, $first: Int!) {
      project(fullPath: \"gitlab-com/team-member-epics/access-requests\") {
        issues(createdAfter: \"2021-12-31\", state: closed, labelName: \"Internal-License\", sort: CREATED_ASC, first: $first, after: $cursor) {
          count
          nodes {
            createdAt
            title
            iid
            totalTimeSpent
            discussions {
              nodes {
                notes {
                  nodes {
                    author {
                      username
                    }
                    body
                  }
                }
              }
            }
          }
          pageInfo {
            endCursor
            hasNextPage
          }
        }
      }
    }
  GRAPHQL

  def initialize(db)
    @db = db
    init_graphql
    init_progress
  end

  def init_graphql
    @graphql = Graphql.new
    @graphql.chunk_size = 100
    @graphql.delay = 0
    @graphql.page_info = method(:page_info)
  end

  def init_progress
    @progress = Progress.new
  end

  def process
    @graphql.collect(QUERY) do |records|
      process_records records
    end
  end

  private

  # Callback function used by Graphql
  def page_info(records)
    records['data']['project']['issues']['pageInfo']
  end

  def issues(records)
    records['data']['project']['issues']['nodes']
  end

  def process_records(records)
    total = records['data']['project']['issues']['count']
    count = issues(records).length
    @progress.show(total, count)
    issues(records).each do |issue|
      process_issue issue
    end
  end

  def process_issue(issue)
    @db.write_issue(issue) if issue['title'].start_with?('GitLab Team Member License')
  end
end
