# frozen_string_literal: true

require 'sqlite3'
require 'json'

# Database table issues
class Issues
  def initialize(db)
    @db = db
  end

  def create_table
    @db.execute create_sql
  end

  def write(record)
    return if record.nil?

    stmt = @db.prepare write_sql
    write_fields stmt, record
    stmt.execute
  end

  private

  def create_sql
    <<~SQL
      create table if not exists issues (
        ticket INT NOT NULL PRIMARY KEY,
        stamp  INT NOT NULL,
        title  TEXT,
        notes  JSON,
        easy   INT
        );
    SQL
  end

  def write_sql
    <<~SQL
      INSERT OR IGNORE INTO issues
      (ticket, stamp, title, notes, easy)
      VALUES
      (:ticket, :stamp, :title, json(:notes), :easy);
    SQL
  end

  def write_fields(stmt, record)
    stmt.bind_params('ticket' => record['iid'])
    stmt.bind_params('stamp' => record['createdAt'])
    stmt.bind_params('title' => record['title'])
    stmt.bind_params('notes' => record['discussions']['nodes'].to_json)
  end

end
