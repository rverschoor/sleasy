# frozen_string_literal: true

# Environment variables used:
# GL_TOKEN=glpat-abc123

require 'faraday'
require 'json'

# Graphql class
class Graphql
  attr_accessor :chunk_size, :delay
  attr_writer :page_info

  def initialize
    connection
    @cursor = ''
    @status = nil
    @json = nil
    @chunk_size = 100
    @delay = 0
  end

  def pretty_json(json)
    JSON.pretty_generate json
  end

  def collect(query)
    loop do
      @more = run_query(query)
      yield @json
      break unless @more

      sleep delay unless delay.zero?
    end
    puts
  end

  private

  def connection
    token = ENV['GL_TOKEN']
    @con = Faraday.new(
      url: 'https://gitlab.com/api/graphql',
      headers: {
        'Content-Type' => 'application/json',
        'Authorization' => "Bearer #{token}"
      }
    )
  end

  def check_valid_response
    @errors = @json['errors']
    return if @status == 200 && @errors.nil?

    puts "status: #{@status}"
    puts "errors: #{@errors}"
    exit 2
  end

  def run_query(query)
    variables = { cursor: @cursor, first: @chunk_size }
    res = @con.post('', { query:, variables: }.to_json)
    @status = res.status
    @json = JSON.parse(res.body)
    check_valid_response
    pagination
    @more
  end

  def pagination
    info = @page_info.call(@json)
    if info.nil?
      @more = false
    else
      @cursor = info['endCursor']
      @more = info['hasNextPage']
    end
  end

  def page_info(_json)
    # Local implementation should return 'pageInfo' record
    # E.g.: json['data']['project']['issues']['pageInfo']
    nil
  end
end
