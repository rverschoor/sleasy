# frozen_string_literal: true

require 'tty-progressbar'

class Progress

  def initialize
    @started = false
  end

  def show(total, count)
    first_progress(total) unless @started
    @bar.advance(count)
  end

  def first_progress(total)
    # Delay progressbar creation until we now total
    @bar = TTY::ProgressBar.new('Slurped [:bar] :current/:total',
                                total:,
                                bar_format: :circle)
    @started = true
  end
end